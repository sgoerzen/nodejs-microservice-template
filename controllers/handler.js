'use strict';

const boom = require('boom'),
	co = require('../common');

let items = new Map();

module.exports = {
	getItem: async function(request) {
		try {
			let item = items.get(request.params.id);
			if (co.isEmpty(item))
				return boom.notFound();
			else
				return item;
		} catch(error) {
			request.log('error', error);
			return boom.badImplementation();
		}
	},
};
