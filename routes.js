'use strict';

const Joi = require('joi'),
	handlers = require('./controllers/handler');

module.exports = function(server) {

	server.route({
		method: 'GET',
		path: '/item/{id}',
		handler: handlers.getItem,
		options: {
			validate: {
				params: {
					id: Joi.string().alphanum().lowercase()
				},
			},
			tags: ['api'],
			description: 'Get an item'
		}
	});

};
