/* eslint promise/no-callback-in-promise: "off" */
'use strict';

//NOTE The following code is only example code!

describe('Example', () => {

	beforeEach((done) => {
		Object.keys(require.cache).forEach((key) => delete require.cache[key]);
		require('chai').should();
		let chai = require('chai');
		let chaiAsPromised = require('chai-as-promised');
		chai.use(chaiAsPromised);
		done();
	});

	let db = new Promise((resolve, reject) => {
		resolve({});
	});

	context('when doing stuff', () => {
		it('should return other stuff', () => {

			return Promise.all([
				db.should.be.fulfilled,
				db.should.eventually.not.be.empty,
				db.should.eventually.have.property('s').that.is.not.empty,
				db.should.eventually.have.property('s').that.has.property('test', 'fulfilled')
			]);
		});

		it('should do even more', () => {
			return db.should.be.fulfilled;
		});
	});

});
