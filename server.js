'use strict';

const hapi = require('hapi'),
	co = require('./common');

let port = (!co.isEmpty(process.env.APPLICATION_PORT)) ? process.env.APPLICATION_PORT : 3000;
const server = new hapi.Server({
	port: port
});

let host = (!co.isEmpty(process.env.VIRTUAL_HOST)) ? process.env.VIRTUAL_HOST : server.info.host;

module.exports = server;

let plugins = [
	require('inert'),
	require('vision'), {
		plugin: require('good'),
		options: {
			ops: {
				interval: 1000
			},
			reporters: {
				console: [{
					module: 'good-squeeze',
					name: 'Squeeze',
					args: [{
						log: '*',
						response: '*',
						request: '*'
					}]
				}, {
					module: 'good-console'
				}, 'stdout']
			}
		}
	}, {
		plugin: require('hapi-swagger'),
		options: {
			host: host,
			info: {
				title: 'Example API',
				description: 'Powered by node, hapi, joi, hapi-swaggered, hapi-swaggered-ui and swagger-ui',
				version: '0.1.0'
			}
		}
	}
];

async function init() {
	await server.register(plugins);
	require('./routes.js')(server);

	await server.start();
	server.log('info', 'Server started at ' + server.info.uri);
}

process.on('unhandledRejection', (err) => {
	console.log(err);
	process.exit(1);
});

init();
